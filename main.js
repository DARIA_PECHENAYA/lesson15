const sum1 = 1 + 2 * 2;
const sum2 = 6 + 7 * 2;
const sum3 = 3 + 4 * 8;
const total = sum1 + sum2 + sum3;

function sum( a, b, c){
    return a + b * c;
}
 const totalByFun = sum(1,2,2) + sum(6,7,2) + sum(3,4,8);
 console.log(totalByFun);


let a = 10;
function foo(a) {
    a = 7;
    console.log(a);
};
foo(a);
console.log(a);


// function sayHello(name){
//     alert('Hello '+ name );
// }
//     sayHello('Daria');


function getMinMax( mass ) {
    let min,max;
    mass.forEach(( item ) => {
        if (min === undefined) {
            min = item;
        } if (max === undefined) {
            max = item;
        } if (min > item) {
            min = item;
        } if (max < item) {
            max = item;
        }
    });
    return {
        min:min,
        max:max
    };
}
console.log(getMinMax([1,2,-17,8,4]));

function getMinMax(mass) {
    return {
        min: Math.min.apply(null,mass),
        max: Math.max.apply(null,mass),
    }
};
console.log(getMinMax([1,2,-17,8,4]));

let obj = {
    name:'123',
    lastname:'248',
    getFullName: function() {
        return this.name + this.lastname;
    }
}; 
console.log(obj.getFullName());
console.log(Object.keys(obj));
console.log(Object.getOwnPropertyNames(obj));
console.log(Object.values(obj));
console.log(obj.hasOwnProperty('text'));
console.log(obj.hasOwnProperty('name'));

const user = {
    name:'text'
};
const test = {...user};
test.name = "new";
console.log(user.name);
console.log(test.name);

let nums = {
    a:1, 
    b:2, 
    c:3
};
console.log(nums['c']);

let objName = {
    Den: 1000,
    Matt: 5000,
    Steve: 2000
};
console.log(objName.hasOwnProperty('Den','Steve'));


 let objDay = {
     1:'mounday',
     2:'вторник',
     3:'среда'
 };
 let day = 3;
 console.log( objDay [day] );
